# Serverless Image Handler

* Run unit tests
```
cd ./deployment
chmod +x ./run-unit-tests.sh
./run-unit-tests.sh
```

* Create Amazon S3 Bucket
```
aws s3 mb s3://my-bucket --region [region]
aws s3 mb s3://my-bucket-[region] --region [region]
```

* Change bucket name that save image replaced
Edit [bucketReplaceImage](source/image-handler/image-handler.js) in file(source/image-handler/image-handler.js)

* Navigate to the deployment folder and build the distributable
```
export DIST_OUTPUT_BUCKET=my-bucket
export TEMPLATE_OUTPUT_BUCKET=my-bucket
export VERSION=my-version
cd ./deployment
./build-s3-dist.sh $DIST_OUTPUT_BUCKET $TEMPLATE_OUTPUT_BUCKET $VERSION
```

* Deploy the distributable to an Amazon S3 bucket in your account (you must have the AWS CLI installed)
```
aws s3 cp ./dist/serverless-image-handler.template s3://$TEMPLATE_OUTPUT_BUCKET/serverless-image-handler/$VERSION/
aws s3 cp ./dist/ s3://$DIST_OUTPUT_BUCKET-[region]/serverless-image-handler/$VERSION/ --recursive --exclude "*" --include "*.zip"
aws s3 cp ./dist/demo-ui-manifest.json s3://$DIST_OUTPUT_BUCKET-[region]/serverless-image-handler/$VERSION/
aws s3 cp ./dist/demo-ui/ s3://$DIST_OUTPUT_BUCKET-[region]/serverless-image-handler/$VERSION/demo-ui/ --recursive --acl bucket-owner-full-control
```

* Get the link of the serverless-image-handler.template uploaded to your Amazon S3 bucket

* Deploy the Serverless Image Handler solution to your account by launching a new AWS CloudFormation stack using the link of the serverless-image-handler.template